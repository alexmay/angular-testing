import { TestBed } from '@angular/core/testing';
import { configureTests } from 'test-config.helper';
import { sum } from './calculator-utils';

it('should sum items', () => {
  const result = sum(1, 2);
  expect(result).toEqual(-1);
});
