import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CounterComponent {
  @Input()
  public counter = 0;

  public increment() {
    this.counter++;
  }

  public decrement() {
    this.counter--;
  }
}
