import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class TestService {
  constructor() {}

  public publicDoStuff() {
    return this.doStuff();
  }
  private doStuff() {
    return 5;
  }
}
