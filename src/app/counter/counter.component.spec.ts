import { fireEvent, render, screen } from '@testing-library/angular';
import { CounterComponent } from './counter.component';

describe('CounterComponent', () => {
  test('should increment the counter', async () => {
    await render(CounterComponent, {});

    const button = screen.getByText(/\+/);
    const counter = screen.getByText(/current count/i);
    fireEvent.click(button);
    expect(counter).toHaveTextContent('1');
  });

  test('should decrement the counter', async () => {
    await render(CounterComponent, {});

    const button = screen.getByText(/\-/);
    const counter = screen.getByText(/current count/i);
    fireEvent.click(button);
    expect(counter).toHaveTextContent('-1');
  });

  test('should increment the counter', async () => {
    await render(CounterComponent, { componentProperties: { counter: 5 } });

    const button = screen.getByText(/\+/);
    const counter = screen.getByText(/current count/i);
    fireEvent.click(button);
    expect(counter).toHaveTextContent('6');
  });
});
